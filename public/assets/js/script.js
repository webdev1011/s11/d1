//Comments

// Comments are section
//

//In Javascript there are two types of comments:
/*
 single ling //
 multi line /**/

//alert("Hello World");

/* syntax and statements

syntax are rules to create statement
statemnts are instructions

statement end with ;
*/
/* variables and constant 
=data containers

variables data that can change
constant is constant

to create var un"let" keyworrd
constant use "const"
*/

let productName = "Desktop computer"; //desktop comp is a string use ""
let productPrice = 18999; // is a number no need for ""
const PI = 3.1416; // is a const because value wont change

/*
rules in naming
var start with small letter for multi us camelcase

var names should describe what value it can contain

/* console - part that the output is displayed 
output and use console.log func
*/

console.log(productName);// is an object .log is a console for wrting output
console.log(productPrice);
console.log(PI);

console.log("hello world");
console.log(1234500);
console.log("i am selling a " +productName);

let fullName = "Brandin B. Brandon";
let schoolName = "Zuitt Coding Bootcamp"
let userName = "brandon00";

let age = 5;
let numberOfPets = 5;
let desiredGrade = 98.5

let isSingle = true; //boolean
let hasEaten = false

let petName; // undefined

let grandChildName = null; // no value

let person = {
	firstName : "Jobert" , // , indicates proterties following the first
	lastName : "Boyd",
	age : 15,
	petName : "Whitey"
};

// operators 
/*5 types
assignment
arithmetic
comparison
relational
logical

ass op uses the = to assign value to var
arithmethic uses + - / *
*/

let num1 = 28;
let num2 = 75;

let sum = num1 + num2;
console.log(sum);
let difference = num1 - num2;
console.log(difference);
let product = num1 * num2;
console.log(product);
let quotient = num2 / num1;
console.log(quotient);
let remainder = num2 % num1;
console.log(remainder);

//combining ass and arit
let num3 = 17;
num3 -= 4; //shorthand
// x = x+y  shorthand x/=y
//special arit operator
 let num4 = 5; // increment and decriment +2 or -1
 num4++;
 console.log(num4);
  // diff num++ and ++num research for tommorow
  // comparisson and relational
  // compares 2 val if = on not
  let numA = 65;
  let numB = 65;
  console.log (numA == numB); //true
let statement1 = "true";// string
let statement2 = true;//bool
console.log(statement1 == statement2); //
console.log(statement1 == statement2); //


//relational compares two num if = or not < > <= >= result t or f
let numC = 25;
let numD = 45;

console.log(numC > numD);//f
console.log(numC < numD);//t
console.log(numC >= numD);//f


//logical comapres 2 bool val
// and (&&), or (||), NOT(!)
let isTall = false;
let isDark = true;
let didPassStandard = isTall && isDark;
console.log(didPassStandard);//= false tandf=f tandt=t

//let didPassStandard = isTall || isDark;
//console.log(didPassStandard);// true t||f=t t rules

console.log(!isTall); // = true  ! reverses value
// yuo can combine compa, rel and logical

let result = isTall || (isDark && isHandsome);
console.log(result); // true

//function reuse or prevent code dupe
// declaration what the func will do
//invication is running
// "funtion" is the keyword

// functions are group of statement to perfome single action to prevent code duoe*/
// 2 main concept
// declaration - define function
//invocation - call use function
//
// function declaratio

function createFullName(fName,mName,lName){
	return Fname + mName + lName
} // parameter  is the input that the function need in order to work
//return is keywoard for the value in function

//also optional to prevent returning val


//function invocation
//let fullName = createFullName("Brandon", "Ray", "Smith");
//console.log(fullName) //BrandonRaySmith
let fulName2 = createFullName("John", "Robert", "Smith");
console.log(fullName2);// JohnRobertSmith
let fN = "Jobert";
let mN = "Bob";
let lN = "Garcia"
let fullName3 = createFullName(fN, mN, lN);
console.log(fullName3);//JobertBobGarcia
let fullName4 = createFullName(mN, fN, lN);
console.log(fullNmae4)//BobJobertGarcia

//mini ex
function addTwo(val1, val2){
	return 2*(val1 + val2);
}